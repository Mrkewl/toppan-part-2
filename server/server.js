const app = require("./app");
const PORT = 8000;
const http = require("http");
const server = http.createServer(app);
const cors = require("cors");


app.use(
    cors({
      origin: "*",
    })
  );
async function startServer() {
  server.listen(PORT, () => {
    console.log(`app listening on port ${PORT}!`);
  });
}
startServer();
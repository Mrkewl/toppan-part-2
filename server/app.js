const { connection } = require("./services/mysql");

const express = require("express");
const cors = require("cors");
const app = express();
// const api = require("./routes/api");
app.use(
  cors({
    origin: "*",
  })
);

app.get("/", (req, res) => {
  connection.connect(function (err) {
    if (err) {
      return console.error("error connecting: " + err.stack);
    }
    console.log("connected as id " + connection.threadId);
  });

  return res.send(JSON.stringify("Received a GET HTTP method"));
});

app.use(express.json());

module.exports = app;

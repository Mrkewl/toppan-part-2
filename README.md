Difficulties:
Not working enough with kubenetes, helm, grafana and prometheus
Reading env variables in a static html - I used react app as a base to read REACT_APP environments.
Finding a way to prove react app can communicate with sql - created a server pod to communicate with mysql pod.
Finding an endpoint to connect server pod to mysql, so i used my local ip to connect to it instead. It is not the right way

Script:
kubectl apply -f mysql2.yaml   
kubectl apply -f frontend.yaml        
kubectl apply -f backend.yaml         

To install prometheus and grafana

brew install helm

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

helm repo update

helm install prometheus prometheus-community/Prometheus

kubectl expose service prometheus-server --type=NodePort --target-port=9090 --name=prometheus-server-ext

helm repo add grafana https://grafana.github.io/helm-charts

helm repo update
